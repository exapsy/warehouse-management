import { LoginPage } from './loginPage';
import { Dashboard } from './dashboard';
import { NotFound } from './notfound';

export { LoginPage, Dashboard, NotFound };