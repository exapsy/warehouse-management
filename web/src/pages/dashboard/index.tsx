import React from 'react';
import Sidebar from '../../components/Sidebar';
import Content from '../../components/DashboardContent';
import './index.scss';

const Dashboard: React.FC = () => {
  return (
    <div className='dashboard'>
      <Sidebar />
      <Content />
    </div>
  );
};

export { Dashboard };