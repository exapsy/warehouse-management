package database

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var client *mongo.Client
var databaseURI string
var databaseName string
var err error

// Initialize setups and runs the database server
func Initialize(databaseURIParam string, databaseNameParam string) {
	// Checking if `client` already exists
	if client != nil {
		log.Println("️⚠ Client already exists - reinitializing database")
	}
	// New client
	client, err = mongo.NewClient(options.Client().ApplyURI(databaseURIParam))
	if err != nil {
		log.Fatal(err)
	}
	// Connect
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	// Verify connection topology
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	// Setting database name variable
	databaseName = databaseNameParam
	databaseURI = databaseURIParam
	// Log approval
	log.Println("✔ Database server running at " + databaseURI + "/" + databaseName)
}

// GetInstance returns an instance of mongodb client
func GetInstance() *mongo.Client {
	if client == nil {
		log.Fatal("MongoDB has not been initialized. Please run database.Initialize()")
	}
	return client
}

// GetDatabase returns a database interface
func GetDatabase() *mongo.Database {
	instance := GetInstance()
	database := instance.Database(databaseName)
	if database == nil {
		log.Fatal("Database `" + databaseName + "` doesn't exist")
	}
	return database
}

// GetDatabaseName returns the database's name
func GetDatabaseName() string {
	// Will return error if client is not connected or database doesn't exist
	GetDatabase()
	return databaseName
}

// GetCollection returns a collection from the database
func GetCollection(collectionName string) *mongo.Collection {
	database := GetDatabase()
	collection := database.Collection(collectionName)
	if collection == nil {
		log.Fatal("Collection `" + collectionName + "` doesn't exist")
	}
	return collection
}
