package models

import (
	"context"
	"log"

	"gitlab.com/exapsy/werehouse-management/internal/app/services/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

// User holds the structure of a user model
type User struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Username string             `bson:"username" json:"username"`
	Password string             `bson:"password" json:"password"`
	Email    string             `bson:"email" json:"email"`
}

// VerifyCredentials returns the corresponding user object if the credentials are correct
func VerifyCredentials(email string, password string) *User {
	collection := database.GetCollection("users")
	userFoundResult := collection.FindOne(context.Background(), &bson.M{"email": email})
	userFound := &User{}
	userFoundResult.Decode(userFound)
	if userFound == (&User{}) {
		return nil
	}
	isUserVerified := userFound.IsPasswordValid(password)
	if !isUserVerified {
		return nil
	}
	return userFound
}

// IsPasswordValid checks if the password is equal to the user's hashed password
func (u *User) IsPasswordValid(pass string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(pass))
	return err == nil
}

// HashPassword hashes the password that is provided
func HashPassword(pass []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pass, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}
