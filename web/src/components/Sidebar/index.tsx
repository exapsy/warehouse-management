import React from 'react';
import Logo from './Logo';
import menuIcon from '../../assets/svg/menuIcon.svg';
import home from '../../assets/svg/home.svg';
import storelogo_1 from '../../assets/svg/store_1.svg';
import storelogo_2 from '../../assets/svg/store_2.svg';
import storelogo_3 from '../../assets/svg/store_3.svg';
import storelogo_4 from '../../assets/svg/store_4.svg';
import settings from '../../assets/svg/settings.svg';
import './index.scss';

const Sidebar: React.FC = () => {
  return (
    <div className='sidebar'>
      <Logo />
      <div className='option-menu' >
        <h2><img src={menuIcon} className='menu-icon' alt='menu' /><span className='menu-header-text' >ΜΕΝΟΥ ΕΠΙΛΟΓΩΝ</span></h2>
        <ul>
          <li className='option-menu-focused'><img src={home} />Κεντρική Σελίδα</li>
          <li><img src={storelogo_1} />Αποθήκη 1</li>
          <li><img src={storelogo_2} />Αποθήκη 2</li>
          <li><img src={storelogo_3} />Αποθήκη 3</li>
          <li><img src={storelogo_4} />Αποθήκη 4</li>
        </ul>
      </div>
      <div className='option-menu' >
        <h2><img src={menuIcon} className='menu-icon' alt='menu' /><span className='menu-header-text' >ΚΑΤΑΧΩΡΙΣΗ ΥΛΙΚΩΝ ΚΑΙ ΚΩΔΙΚΩΝ</span></h2>
        <ul>
          <li><img src={storelogo_1} /> Καταχώρηση</li>
        </ul>
      </div>
      <div className='settings'>
        <img src={settings} />
        <span>ΡΥΘΜΙΣΕΙΣ</span>
      </div>
    </div>
  );
};

export default Sidebar;