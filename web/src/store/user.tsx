import {
  observable
} from 'mobx';
import { createContext } from 'react';
import axios from 'axios';

interface ProfileResponse {
  data: Profile;
  error: any;
}
interface Profile {
  username: string;
  email: string;
}

export class UserStore {
  constructor() {
    this.fetchSession();
  }

  /**
   *
   * Keeps track of user's profile
   * 
   * If user doesnt exist, fields are empty
   * @type {Profile}
   * @memberof UserStore
   */
  @observable profile: Profile = { username: '', email: '' };
  /**
   * 
   * Keeps track of the user session state
   * @memberof UserStore
   */
  @observable loggedOn = false;

  /**
   * 
   * fetchSession fetches the client's user session
   * 
   * Affects the `loggedOn` state variable
   * @memberof UserStore
   */
  async fetchSession() {
    // Fetch profile
    const response = (await axios.get(
      'http://localhost:' + process.env.REACT_APP_API_PORT + '/profile',
      {
        withCredentials: true,
        headers: {
          'Access-Control-Allow-Origin': 'http://localhost:' + process.env.PORT,
        }
      })).data;
    this.loggedOn = response.data !== undefined;
    if (this.loggedOn) {
      const profileResponse: ProfileResponse = response.data as unknown as ProfileResponse;
      this.profile = profileResponse.data;
    }
  }
}

export default createContext(new UserStore())