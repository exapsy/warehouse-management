package controllers

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/exapsy/werehouse-management/internal/app/models"
	"gitlab.com/exapsy/werehouse-management/internal/app/services/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type filterBody struct {
	Email    string `json:"email,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type putUserBody struct {
	Email    string `bson:"email,omitempty" json:"email,omitempty"`
	Username string `bson:"username,omitempty" json:"username,omitempty"`
	Password string `bson:"password,omitempty" json:"password,omitempty"`
}

type putUsersBody struct {
	Filter filterBody  `json:"filter"`
	Put    models.User `json:"put"`
}

type deleteUsersBody struct {
	Ids       []string `json:"ids,oemitempty"`
	Emails    []string `json:"emails,oemitempty"`
	Usernames []string `json:"usernames,oemitempty"`
}

type postUsersBody struct {
	Data []models.User `json:"data"`
}

func getUsers(w http.ResponseWriter, r *http.Request) {
	// Get collection
	collection := database.GetCollection("users")
	// Find all users
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	cur, err := collection.Find(ctx, &bson.M{})
	if err != nil {
		sendError(w, 400, err.Error())
		return
	}
	defer cur.Close(context.Background())
	// Users array
	users := []models.User{}
	// Iterate through all the users
	for cur.Next(context.Background()) {
		user := &models.User{}
		err = cur.Decode(user)
		if err != nil {
			sendError(w, 400, err.Error())
			return
		}
		// Append to user array
		users = append(users, *user)
	}
	// Cursor error check
	if err = cur.Err(); err != nil {
		sendError(w, 400, err.Error())
		return
	}
	// Send json
	sendData(w, 200, users)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	// Error preperation
	var invalidParamsError = httpInvalidParams{}
	// Get user id
	vars := mux.Vars(r)
	userID := vars["userid"]
	objectUserID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		invalidParamsError.InvalidParams = map[string]interface{}{
			"userid": "Not an ObjectID",
		}
	}
	// Get collection
	collection := database.GetCollection("users")
	// Find user
	result := collection.FindOne(context.Background(), &bson.M{"_id": objectUserID})
	// Store to user struct
	user := models.User{}
	result.Decode(&user)
	// Send json
	if len(invalidParamsError.InvalidParams) != 0 {
		invalidParamsError.Title = "Invalid parameters"
		invalidParamsError.Details = "Some parameters given were invalid"
		sendError(w, 400, invalidParamsError)
		return
	}
	if user == (models.User{}) {
		httpErr := httpError{Title: "User doesn't exist", Details: "The user with object id " + userID + " doesn't exist"}
		sendError(w, 400, httpErr)
		return
	}
	sendData(w, 200, user)
}

func putUsers(w http.ResponseWriter, r *http.Request) {
	// Get collection
	collection := database.GetCollection("users")
	// Get request body
	body := putUsersBody{}
	json.NewDecoder(r.Body).Decode(&body)
	// Error check
	if body.Filter.Email == "" && body.Filter.Username == "" {
		httpErr := httpError{Title: "No filter was given", Details: "No filter was given and updating all user base is not allowed"}
		sendError(w, 400, httpErr)
		return
	}
	if body.Put.Username == "" && body.Put.Email == "" {
		httpErr := httpError{Title: "No put body was given", Details: "Can't update anything if no update instructions are given, provide what fields have to be changed in the put field"}
		sendError(w, 400, httpErr)
		return
	}
	// Update
	result := collection.FindOneAndUpdate(context.Background(), bson.M{"email": body.Filter.Email}, bson.M{"username": body.Put.Username})

	user := models.User{}
	result.Decode(&user)
	sendData(w, 200, user.ID)
}

func putUser(w http.ResponseWriter, r *http.Request) {
	// Error preparation
	invalidParamsError := httpInvalidParams{}
	// Get collection
	collection := database.GetCollection("users")
	// Get request body
	body := putUserBody{}
	json.NewDecoder(r.Body).Decode(&body)
	// Get userid
	vars := mux.Vars(r)
	userid := vars["userid"]
	objectIDUserID, err := primitive.ObjectIDFromHex(userid)
	// Error check
	if err != nil {
		invalidParamsError.InvalidParams = map[string]interface{}{
			"userid": "Not an object id",
		}
	}
	if body.Email == "" && body.Username == "" && body.Password == "" {
		httpErr := httpError{Title: "Insufficient data", Details: "No update body was given"}
		sendError(w, 400, httpErr)
		return
	}
	if len(invalidParamsError.InvalidParams) != 0 {
		invalidParamsError.Title = "Invalid parameters"
		invalidParamsError.Details = "Some parameters were not given or were invalid"
		sendError(w, 400, invalidParamsError)
		return
	}
	// Update user
	foundUserRes := collection.FindOne(context.Background(), &bson.M{"_id": objectIDUserID})
	foundUser := models.User{}
	foundUserRes.Decode(&foundUser)

	result := collection.FindOneAndUpdate(
		context.Background(),
		&bson.M{"_id": objectIDUserID},
		&bson.M{"$set": body})
	userUpdated := models.User{}
	result.Decode(&userUpdated)
	if userUpdated == (models.User{}) {
		httpErr := httpError{Title: "User doesn't exist", Details: "No user exists with user id " + userid}
		sendError(w, 400, httpErr)
		return
	}
	sendData(w, 200, userUpdated.ID)
}

func postUsers(w http.ResponseWriter, r *http.Request) {
	// Get collection
	collection := database.GetCollection("users")
	// Get request body
	body := postUsersBody{}
	json.NewDecoder(r.Body).Decode(&body)
	// Error check
	if len(body.Data) == 0 {
		httpErr := httpError{Title: "Insufficient data", Details: "No data was provided to request body"}
		sendError(w, 400, httpErr)
		return
	}
	for _, user := range body.Data {
		if user.Username == "" || user.Password == "" || user.Email == "" {
			httpErr := httpError{Title: "Forgotted parameters", Details: "Some parameters in some users were not provided"}
			sendError(w, 400, httpErr)
			return
		}
		usernameFoundSingleResult := collection.FindOne(context.Background(), bson.M{"username": user.Username})
		usernameFoundUser := models.User{}
		usernameFoundSingleResult.Decode(&usernameFoundUser)
		if usernameFoundUser != (models.User{}) {
			httpErr := httpError{Title: "Username already exists", Details: "A user with username " + user.Username + " already exists"}
			sendError(w, 400, httpErr)
			return
		}
		emailFoundSingleResult := collection.FindOne(context.Background(), bson.M{"email": user.Email})
		emailFoundUser := models.User{}
		emailFoundSingleResult.Decode(&emailFoundUser)
		if emailFoundUser != (models.User{}) {
			httpErr := httpError{Title: "Email already exists", Details: "A user with email " + user.Email + " already exists"}
			sendError(w, 400, httpErr)
			return
		}
	}
	// Users interface
	users := make([]interface{}, len(body.Data))
	for index, user := range body.Data {
		// Add objectID
		user.ID = primitive.NewObjectID()
		user.Password = models.HashPassword([]byte(user.Password))
		users[index] = user
	}
	results, err := collection.InsertMany(context.Background(), users)
	if err != nil {
		httpErr := httpError{Title: "Insertion failed", Details: err.Error()}
		sendError(w, 500, httpErr)
		return
	}
	sendData(w, 201, results.InsertedIDs)
}

func deleteUsers(w http.ResponseWriter, r *http.Request) {
	// Error preparation
	invalidParamsErr := httpInvalidParams{}
	invalidParamsErr.InvalidParams = map[string]interface{}{}
	// Get collection
	collection := database.GetCollection("users")
	// Get body
	body := deleteUsersBody{}
	json.NewDecoder(r.Body).Decode(&body)
	var ids []primitive.ObjectID
	emails := body.Emails
	usernames := body.Usernames
	for _, userid := range body.Ids {
		objectIDUserID, err := primitive.ObjectIDFromHex(userid)
		if err != nil {
			invalidParamsErr.InvalidParams[userid] = "Not an object ID"
			continue
		}
		ids = append(ids, objectIDUserID)
	}
	if len(invalidParamsErr.InvalidParams) != 0 {
		invalidParamsErr.Title = "Invalid parameters"
		invalidParamsErr.Details = "Some parameters were either invalid or not given"
		sendError(w, 400, invalidParamsErr)
		return
	}
	// Delete users
	deletedResult, err := collection.DeleteMany(
		context.Background(),
		&bson.M{
			"$or": []interface{}{
				bson.M{
					"_id": &bson.M{
						"$in": ids,
					},
				},
				bson.M{
					"username": &bson.M{
						"$in": usernames,
					},
				},
				&bson.M{
					"email": &bson.M{
						"$in": emails,
					},
				},
			}})
	if err != nil {
		log.Println(err)
		sendError(w, 400, err)
		return
	}
	sendData(w, 200, deletedResult)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	// Error preparation
	invalidParamsError := httpInvalidParams{}
	// Get collection
	collection := database.GetCollection("users")
	// Get user id
	vars := mux.Vars(r)
	userid := vars["userid"]
	objectIDUserID, err := primitive.ObjectIDFromHex(userid)
	if err != nil {
		invalidParamsError.InvalidParams = map[string]interface{}{
			"userid": "Not an object id",
		}
	}
	// Delete user
	userDeletedResult := collection.FindOneAndDelete(context.Background(), &bson.M{"_id": objectIDUserID})
	userDeleted := models.User{}
	userDeletedResult.Decode(&userDeleted)
	if userDeleted == (models.User{}) {
		httpErr := httpError{Title: "User doesn't exist", Details: "No user was found with " + userid + " Object ID"}
		sendError(w, 400, httpErr)
		return
	}
	sendData(w, 200, userDeleted)
}
