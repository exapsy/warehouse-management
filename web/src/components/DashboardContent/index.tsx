import React from 'react';
import SearchBox from '../SearchBox';
import redEllipse from '../../assets/svg/red_ellipse.svg';
import yellowEllipse from '../../assets/svg/yellow_ellipse.svg';
import greenEllipse from '../../assets/svg/green_ellipse.svg';
import './index.scss';

const DashboardContent: React.FC = () => {
  return (
    <div className='content'>
      <header>
        <h1>Κεντρική σελίδα</h1>
        <div className='content-header-side'>
          <SearchBox />
        </div>
      </header>
      <div className='main-content'>
        <span className='action-grid-item'>
          <img src={redEllipse} />
          <div className='action-grid-item-content'>
            <h2>ΕΞΑΓΩΓΗ</h2>
            <p>Εξαγωγή υλικών ή ρεταλιών απο την αποθήκη</p>
          </div>
        </span>
        <span className='action-grid-item'>
          <img src={yellowEllipse} />
          <div className='action-grid-item-content'>
            <h2>ΜΕΤΑΚΙΝΗΣΗ</h2>
            <p>Εξαγωγή υλικών ή ρεταλιών απο την αποθήκη</p>
          </div>
        </span>
        <span className='action-grid-item'>
          <img src={greenEllipse} />
          <div className='action-grid-item-content'>
            <h2>ΕΙΣΑΓΩΓΗ</h2>
            <p>Εξαγωγή υλικών ή ρεταλιών απο την αποθήκη</p>
          </div>
        </span>
      </div>
    </div >
  );
}

export default DashboardContent;