import React from 'react';
import { Page, IProps as PageProps } from './Page';
import { BrowserRouter as Router } from 'react-router-dom';

interface IProps {
	children?: React.ReactElement<PageProps>[] | React.ReactElement<PageProps>;
	loginPage?: string;
	notFoundPage?: string;
}

const PageSelector: React.FC<IProps> = (props) => {
	if (!props.children) {
		throw new Error('No pages were provided to PageSelector component');
	}

	/* WARNING:
	 * Do not change the order of next two commands 
	 * 	In case `notFoundPage` also requires authorization,
	 * 	we still need to redirect it to `loginPage`
	 */

	// If page doesn't exist redirect to /{notFoundPage}
	checkNotFound(props);
	// If page requires authorization, redirect to /{loginPage}
	checkAuth(props);

	return (
		<Router>
			<div className='route' style={{ height: '100%' }}>
				{props.children}
			</div>
		</Router>
	);
};

// Redirects if page pathname doesn't exist
const checkNotFound = (pageSelectorProps: IProps): void => {
	if (pageSelectorProps.children) {
		const pages = getPages(pageSelectorProps.children);
		const pathnames = getPagesPathnames(pages);
		const currentPathname = window.location.pathname;
		const isPathnameExistant = pathnames.includes(currentPathname);
		if (!isPathnameExistant && pageSelectorProps.notFoundPage) {
			window.location.assign(pageSelectorProps.notFoundPage)
		}
	}
}

// Redirects to login if not authorized and login auth exists
const checkAuth = (pageSelectorProps: IProps): void => {
	if (pageSelectorProps.loginPage && pageSelectorProps.children) {
		const authRoutes = getAuthRoutesFromChildren(pageSelectorProps.children);
		const currentPathname = window.location.pathname;
		const isPathnameAuth = authRoutes.includes(currentPathname);

		if (isPathnameAuth) {
			window.location.assign(pageSelectorProps.loginPage);
		}
	}
}

// Returns all pages with page.auth=true directly from children
const getAuthRoutesFromChildren = (children: React.ReactElement<PageProps>[] | React.ReactElement<PageProps>): string[] => {
	let pages: PageProps[];
	let authRoutes: string[];
	pages = getPages(children);
	authRoutes = getAuthRoutes(pages);

	return authRoutes;
};

// Returns PageProps from each child in the PageSelector
const getPages = (children: React.ReactElement<PageProps>[] | React.ReactElement<PageProps>): PageProps[] => {
	const pages: PageProps[] = [];
	React.Children.forEach(children, (child, index) => {
		pages[index] = child.props;
	})

	return pages;
};

// Returns all pages where page.auth=true
const getAuthRoutes = (pages: PageProps[]): string[] => {
	const authRoutes: string[] = [];
	pages.forEach(page => {
		if (page.auth) {
			authRoutes.push(page.path);
		}
	});

	return authRoutes;
};

// Returns all pathnames from pages
const getPagesPathnames = (pages: PageProps[]): string[] => {
	const pagesPathnames: string[] = [];
	pages.forEach(page => {
		pagesPathnames.push(page.path);
	});

	return pagesPathnames;
};
export { PageSelector, Page };