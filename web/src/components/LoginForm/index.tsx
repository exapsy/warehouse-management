import React, { useState } from 'react';
import './index.scss';

interface IProps {
	onSubmit: (username: string, password: string) => void
}

const LoginForm: React.FC<IProps> = (props) => {
	const [username, setUsername] = useState("")
	const [password, setPassword] = useState("")
	const submitOnClick = (event: React.MouseEvent<HTMLInputElement>) => {
		props.onSubmit(username, password)
	}
	return (
		<div className='LoginForm'>
			<div className='container'>
				<h1>ΣΥΝΔΕΣΗ</h1>
				<div id='email'>
					<h2>Δ/νση ηλεκτρονικού ταχυδρομείου</h2>
					<input
						id='email'
						type='text'
						name='email'
						onChange={
							(event: React.ChangeEvent<HTMLInputElement>) => setUsername(event.currentTarget.value)
						}
					/>
				</div>
				<div id='password'>
					<h2>Κωδικός πρόσβασης</h2>
					<input
						id='password'
						type='password'
						name='password'
						onChange={
							(event: React.ChangeEvent<HTMLInputElement>) => setPassword(event.currentTarget.value)
						}
					/>
				</div>
				<div id='login'>
					<input value='Είσοδος' type='submit' onClick={submitOnClick} />
				</div>
				<div id='forgotPassword'>
					<a href='http://localhost:3000/#'>Ξέχασα τον κωδικό μου</a>
				</div>
			</div>
		</div>
	)
};

export { LoginForm };