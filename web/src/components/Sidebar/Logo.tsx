import React from 'react';
import logo from '../../assets/svg/logo.svg';
import './Logo.scss';

const Logo: React.FC = () => {
  return (
    <div className='logo-header'>
      <img src={logo} className='app-logo' alt='logo' /> 
      <h1>Warehouse management</h1>
    </div>
  );
};

export default Logo;