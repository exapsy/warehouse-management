package controllers

import (
	"context"
	"net/http"

	"github.com/gorilla/securecookie"
	"gitlab.com/exapsy/werehouse-management/internal/app/models"
	"gitlab.com/exapsy/werehouse-management/internal/app/services/database"
	"go.mongodb.org/mongo-driver/bson"
)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func postLogin(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	email := r.FormValue("email")
	password := r.FormValue("password")
	isEmail := len(email) != 0
	isPassword := len(password) != 0

	if !isEmail && !isPassword {
		httpErr := httpError{Title: "Invalid parameters", Details: "Email and/or password were empty"}
		sendError(w, 400, httpErr)
		return
	}

	verifiedUser := models.VerifyCredentials(email, password)
	isUserVerified := verifiedUser != nil
	if !isUserVerified {
		httpErr := httpError{Title: "Invalid Credentials", Details: "Username or password was wrong"}
		sendError(w, 200, httpErr)
		return
	}
	setCookie(verifiedUser.Username, w)
	sendData(w, 200, verifiedUser)
}

func getLogout(w http.ResponseWriter, r *http.Request) {
	if username := getCookieUsername(r); username == "" {
		sendData(w, 200, "Not logged in")
		return
	}
	clearCookie(w)
	sendData(w, 200, "Logout")
}

func getProfile(w http.ResponseWriter, r *http.Request) {
	username := getCookieUsername(r)
	collection := database.GetCollection("users")
	userResult := collection.FindOne(context.Background(), &bson.M{"username": username})
	user := models.User{}
	userResult.Decode(&user)
	if user == (models.User{}) {
		sendError(w, 200, "Not logged in")
		return
	}
	sendData(w, 200, user)
}

func setCookie(username string, w http.ResponseWriter) {
	value := map[string]string{
		"name": username,
	}
	if encoded, err := cookieHandler.Encode("cookie", value); err == nil {
		cookie := &http.Cookie{
			Name:  "cookie",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(w, cookie)
	}
}

func clearCookie(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "cookie",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
}

func getCookieUsername(r *http.Request) (username string) {
	if cookie, err := r.Cookie("cookie"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("cookie", cookie.Value, &cookieValue); err == nil {
			username = cookieValue["name"]
		}
	}
	return username
}
