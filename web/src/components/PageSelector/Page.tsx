import React from 'react';
import { Route } from 'react-router-dom';

// TODO: change `render` type to an appropriate react component type
export interface IProps {
    path: string;
    exact?: boolean;
    auth?: boolean;
    render: any;
};

const Page: React.FC<IProps> = (props) => {
    const { path, exact = false, auth = false, render } = props;
    return (
        <Route path={path} exact={exact} component={render} />
    );
};

export { Page };