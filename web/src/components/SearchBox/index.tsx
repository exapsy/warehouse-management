import React from 'react';
import search from '../../assets/svg/search.svg';
import './index.scss';

const SearchBox: React.FC = () => {
  return (
    <div className='searchbox'>
      <img src={search} />
      <input type='text' placeholder='Αναζήτηση υλικού, έλεγχος αποθεμάτων' />
    </div>
  )
};

export default SearchBox;