package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type errorResponse struct {
	Error interface{} `json:"error"`
}
type response struct {
	Data interface{} `json:"data"`
}
type httpInvalidParams struct {
	httpError
	InvalidParams map[string]interface{} `json:"invalidParams"`
}
type httpError struct {
	Title   string `json:"title"`
	Details string `json:"details"`
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		log.Println(r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

// RouteHandlers imports all the required controllers to the main router
func RouteHandlers(r *mux.Router) {
	// Root route
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		message := "Welcome to werehouse-management API\nConsider reading the OpenAPI specification for a more exclusive experience\nhttps://gitlab.com/exapsy/werehouse-management/tree/master/api"
		w.Write([]byte(message))
	})
	// Middlewares
	r.Use(loggingMiddleware)
	// Ping
	r.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		message := "pong"
		w.Write([]byte(message))
	})
	// Users
	r.HandleFunc("/users", getUsers).Methods("GET")
	r.HandleFunc("/users", putUsers).Methods("PUT")
	r.HandleFunc("/users", postUsers).Methods("POST")
	r.HandleFunc("/users", deleteUsers).Methods("DELETE")
	r.HandleFunc("/users/{userid}", getUser).Methods("GET")
	r.HandleFunc("/users/{userid}", putUser).Methods("PUT")
	r.HandleFunc("/users/{userid}", deleteUser).Methods("DELETE")
	// User Session
	r.HandleFunc("/login", postLogin).Methods("POST")
	r.HandleFunc("/logout", getLogout).Methods("GET")
	r.HandleFunc("/profile", getProfile).Methods("GET")
}

// sendData wraps the data interface and sends it as an http json response
func sendData(w http.ResponseWriter, statusCode int, data interface{}) {
	wrappedData := response{Data: data}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(wrappedData)
}

// sendError sends a message wrapped in an error format
func sendError(w http.ResponseWriter, statusCode int, err interface{}) {
	wrappedError := errorResponse{Error: err}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(wrappedError)
}
