package app

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/exapsy/werehouse-management/internal/app/services/api"
	"gitlab.com/exapsy/werehouse-management/internal/app/services/database"
)

// Application holds and initiates the software structure
type Application struct {
	Port         string
	WebPort      string
	DatabaseURI  string
	DatabaseName string
}

// Default values
const defaultPort string = "8080"
const defaultWebPort string = "3000"
const defaultDatabaseURI string = "mongodb://localhost:27017"
const defaultDatabaseName string = "test"

// Initialize initiates an API and a Database server
// which are meant to cooperate and communicate with each other
func (a *Application) Initialize() {
	a.initializeVariables()
	database.Initialize(a.DatabaseURI, a.DatabaseName)
	api.Initialize(a.Port, a.WebPort)
}

// initializeVariables initiates all the application variables
// they're taken from the environment variables,
// which if non-existant are replaced by default values instead
func (a *Application) initializeVariables() {
	// Load .env file(s) if existant
	godotenv.Load(".env")
	// Get env vars
	a.Port = os.Getenv("REACT_APP_API_PORT")
	a.DatabaseURI = os.Getenv("DATABASE_URI")
	a.DatabaseName = os.Getenv("DATABASE_NAME")
	a.WebPort = os.Getenv("PORT")
	// Typechecking
	// Setting vars to default values wherever invalid values were given
	if len(a.Port) == 0 {
		log.Println("⚠️ Environment variable `REACT_APP_API_PORT` was not given")
		a.Port = defaultPort
	}
	if len(a.DatabaseURI) == 0 {
		log.Println("⚠️ Environment variable `DATABASE_URI` was not given")
		a.DatabaseURI = defaultDatabaseURI
	}
	if len(a.DatabaseName) == 0 {
		log.Println("⚠️ Environment variable `DATABASE_NAME` was not given")
		a.DatabaseName = defaultDatabaseName
	}
	if len(a.WebPort) == 0 {
		log.Println("⚠️ Environment variable `PORT` was not given")
		a.DatabaseName = defaultWebPort
	}
	// Log values
	log.Println("✔ REACT_APP_API_PORT = " + a.Port)
	log.Println("✔ DATABASE_URI = " + a.DatabaseURI)
	log.Println("✔ DATABASE_NAME = " + a.DatabaseName)
	log.Println("✔ PORT = " + a.WebPort)
}
