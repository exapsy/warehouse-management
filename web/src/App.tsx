import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { LoginPage, Dashboard } from './pages';
import './App.scss';
import UserStore from './store/user';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

const App = observer(() => {
  const store = useContext(UserStore);
  console.log('Is logged on', store.loggedOn);
  return (
    <div className="App">
      <Router>
        <Route
          path='/login'
          render={
            () =>
              !store.loggedOn
                ? <LoginPage />
                : <Redirect to='/' />
          }
        />

        <Route
          path='/'
          exact={true}
          render={
            () =>
              store.loggedOn
                ? <Dashboard />
                : <Redirect to='/login' />
          }
        />
      </Router>
    </div>
  );
});


export default App;
