package main

import (
	"gitlab.com/exapsy/werehouse-management/internal/app"
)

func main() {
	app := &app.Application{}
	app.Initialize()
}
