- [Warehouse management](#warehouse-management)
  - [Requirements](#requirements)
  - [How to run](#how-to-run)
    - [Backend](#backend)
    - [Frontend](#frontend)
    - [API specification](#api-specification)
  - [Project layout](#project-layout)
  - [Other files](#other-files)
  - [Environment variables](#environment-variables)
    - [Example](#example)

# Warehouse management
Implementation of a mockup. 
https://www.figma.com/file/a5UnGjNypPyV2FCrV9qOYbHz/Training?node-id=0%3A1

Made for an interview, with love.

## Requirements

- **Go** (Golang)
- **NodeJS**
- **Yarn** (You can use npm, but I do not guarantee the dependencies' version will be compliant with the app)
- **MongoDB** server running (Port is settable in .env file, otherwise, default port is used)

## How to run

### Backend
```bash
go run ./main.go
```

### Frontend
```bash
cd ./web
yarn install
yarn start
```

### API specification
```bash
cd ./api
yarn install
yarn start
```

## Project layout
- **API** is the the API specification
- **INTERNAL** is the Backend implementation
- **WEB** is the Frontend application in React
- **VENDOR** holds all the go modules

## Other files
- **main.go** is the backend initialization and start script
- **go.mod** holds the application's name and module names & versions
- **go.sum** holds all the required dependencies

## Environment variables
Create a `.env` file with any of the following properties. Those which are not provided will get a default value
- **REACT_APP_API_PORT**: Backend tcp port
  - **default**: 8080
- **DATABASE_URI**: MongoDB database host
  - **default**: localhost:27017
- **DATABASE_NAME**: MongoDB database name
  - **default**: test
- **PORT**: Frontend web tcp port
  - **default**: 3000 

### Example
```.env
# .env
REACT_APP_API_PORT=8080
DATABASE_URI=localhost:70181
```