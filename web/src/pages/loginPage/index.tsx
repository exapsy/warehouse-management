import React, { useContext } from 'react';
import { LoginForm } from '../../components/LoginForm';
import axios from 'axios';
import loginArt from '../../assets/svg/loginArt.svg';
import './index.scss';
import { observer } from 'mobx-react-lite';
import UserStore from '../../store/user';

interface ProfileResponse {
	data: Profile;
	error: any;
}
interface Profile {
	username: string;
	email: string;
}

const LoginPage = observer(() => {
	const store = useContext(UserStore);
	// LoginFormSubmitHandler handles the login submit button when pushed
	// Sends a post request to login via the backend server
	const LoginFormSubmitHandler = async (username: string, password: string) => {
		const formBody = [];
		const encodedUsername = encodeURIComponent(username);
		const encodedPassword = encodeURIComponent(password);
		formBody.push("email=" + encodedUsername);
		formBody.push("password=" + encodedPassword);
		const formBodyString = formBody.join("&");
		const response = await axios.post(
			'http://localhost:' + process.env.REACT_APP_API_PORT + '/login',
			formBodyString,
			{
				withCredentials: true,
				headers: {
					'Access-Control-Allow-Origin': 'http://localhost:' + process.env.PORT,
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}
		);
		const loggedOn = response.data != null;
		if (loggedOn) {
			const profileFetch: ProfileResponse = response.data as unknown as ProfileResponse;
			store.profile = profileFetch.data;
			store.loggedOn = true;
		} else {
			store.loggedOn = false;
		}
	}
	return (
		<div className='LoginPage'>
			<img src={loginArt} className='LoginDecoration' alt='login-art' />
			<div className='login-content'>
				<LoginForm onSubmit={LoginFormSubmitHandler} />
			</div>
		</div>
	);
});

export { LoginPage };