package api

import (
	"log"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/exapsy/werehouse-management/internal/app/controllers"
)

// Initialize setups and runs the API server
func Initialize(port string, webPort string) {
	// Mux
	r := mux.NewRouter()
	// Trims trailing slash
	r.StrictSlash(true)
	// Route Controllers
	controllers.RouteHandlers(r)
	// Use CORS
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:" + webPort},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"},
		AllowedHeaders:   []string{"Access-Control-Allow-Origin", "Access-Control-Allow-Methods"},
		AllowCredentials: true,
	})
	handler := c.Handler(r)
	// Run HTTP server
	uri := "localhost:" + port
	listener, err := net.Listen("tcp", uri)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("✔ API server running at http://localhost:" + port)
	err = http.Serve(listener, handler)
	if err != nil {
		log.Fatal(err)
	}
}
